[package]
name = "simple_path_tracer" # If you change this name, change it also in index.html and build.sh
version = "1.0.0"
authors = ["wgpu developers", "Alpi Tolvanen <alpi.tolvanen@pm.me>",]
edition = "2021"
description = "A small example of webgpu computing and rendering. Based on wgpu examples."
keywords = ["graphics"]
license = "MIT OR Apache-2.0"

[dependencies]
# futures = "0.3"
wgpu = "0.14"
winit = "0.27"
# rand = "0.8.3"
# cgmath = "0.18"
bytemuck = { version = "1.4", features = ["derive"] }
log = "0.4"
glam = "0.22"
futures-intrusive = "0.5.0"

[target.'cfg(not(target_arch = "wasm32"))'.dependencies]
#subscriber = {version = "0.1", package = "wgpu-subscriber"}
env_logger = "0.8"
async-executor = "1.0"
pollster = "0.2"
instant = "0.1.10"  # This is std::instant that works also in web
image = "0.23.14"
dialoguer = "0.8.0"  # Select gpu cli

[target.'cfg(target_arch = "wasm32")'.dependencies]
wasm-bindgen = "0.2.83"
web-sys = { version = "0.3.60", features = [
    "Document",
    "Navigator",
    "Node",
    "NodeList",
    "Gpu",
    "GpuAdapter",
    "GpuAddressMode",
    "GpuBindGroup",
    "GpuBindGroupDescriptor",
    "GpuBindGroupEntry",
    "GpuBindGroupLayout",
    "GpuBindGroupLayoutDescriptor",
    "GpuBindGroupLayoutEntry",
    "GpuBlendComponent",
    "GpuBlendFactor",
    "GpuBlendOperation",
    "GpuBlendState",
    "GpuBuffer",
    "GpuBufferBinding",
    "GpuBufferBindingLayout",
    "GpuBufferBindingType",
    "GpuBufferDescriptor",
    "GpuCanvasContext",
    "GpuCanvasConfiguration",
    "GpuColorDict",
    "GpuColorTargetState",
    "GpuCommandBuffer",
    "GpuCommandBufferDescriptor",
    "GpuCommandEncoder",
    "GpuCommandEncoderDescriptor",
    "GpuCompareFunction",
    "GpuCompilationInfo",
    "GpuCompilationMessage",
    "GpuCompilationMessageType",
    "GpuComputePassDescriptor",
    "GpuComputePassEncoder",
    "GpuComputePipeline",
    "GpuComputePipelineDescriptor",
    "GpuCullMode",
    "GpuDepthStencilState",
    "GpuDevice",
    "GpuDeviceDescriptor",
    "GpuDeviceLostInfo",
    "GpuDeviceLostReason",
    "GpuErrorFilter",
    "GpuExtent3dDict",
    "GpuFeatureName",
    "GpuFilterMode",
    "GpuFragmentState",
    "GpuFrontFace",
    "GpuImageCopyBuffer",
    "GpuImageCopyTexture",
    "GpuImageDataLayout",
    "GpuIndexFormat",
    "GpuLoadOp",
    "GpuMultisampleState",
    "GpuObjectDescriptorBase",
    "GpuOrigin2dDict",
    "GpuOrigin3dDict",
    "GpuOutOfMemoryError",
    "GpuPipelineDescriptorBase",
    "GpuPipelineLayout",
    "GpuPipelineLayoutDescriptor",
    "GpuPowerPreference",
    "GpuPrimitiveState",
    "GpuPrimitiveTopology",
    "GpuProgrammableStage",
    "GpuQuerySet",
    "GpuQuerySetDescriptor",
    "GpuQueryType",
    "GpuQueue",
    "GpuRenderBundle",
    "GpuRenderBundleDescriptor",
    "GpuRenderBundleEncoder",
    "GpuRenderBundleEncoderDescriptor",
    "GpuRenderPassColorAttachment",
    "GpuRenderPassDepthStencilAttachment",
    "GpuRenderPassDescriptor",
    "GpuRenderPassEncoder",
    "GpuRenderPipeline",
    "GpuRenderPipelineDescriptor",
    "GpuRequestAdapterOptions",
    "GpuSampler",
    "GpuSamplerBindingLayout",
    "GpuSamplerBindingType",
    "GpuSamplerDescriptor",
    "GpuShaderModule",
    "GpuShaderModuleDescriptor",
    "GpuStencilFaceState",
    "GpuStencilOperation",
    "GpuStorageTextureAccess",
    "GpuStorageTextureBindingLayout",
    "GpuStoreOp",
    "GpuSupportedFeatures",
    "GpuSupportedLimits",
    "GpuTexture",
    "GpuTextureAspect",
    "GpuTextureBindingLayout",
    "GpuTextureDescriptor",
    "GpuTextureDimension",
    "GpuTextureFormat",
    "GpuTextureSampleType",
    "GpuTextureView",
    "GpuTextureViewDescriptor",
    "GpuTextureViewDimension",
    "GpuUncapturedErrorEvent",
    "GpuUncapturedErrorEventInit",
    "GpuValidationError",
    "GpuVertexAttribute",
    "GpuVertexBufferLayout",
    "GpuVertexFormat",
    "GpuVertexState",
    "GpuVertexStepMode",
    "HtmlCanvasElement",
    "OffscreenCanvas",
    "Window",
]}
js-sys = "0.3.54"
wasm-bindgen-futures = "0.4.33"
instant = { version = "0.1.10", features = [ "wasm-bindgen" ] }
# enable parking_lot's wasm-bindgen feature so that it, in turn, enables that of crate `instant`
parking_lot = { version = "0.11", features = ["wasm-bindgen"] }
console_error_panic_hook = "0.1.6"
console_log = "0.1.2"

[build-dependencies]
anyhow = "1.0"

[profile.release]
lto = "fat"  # For getting linux binary to be <10MB so that I can upload it to GitLab
