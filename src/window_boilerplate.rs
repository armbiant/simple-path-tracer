// This is based on wgpu repo's example's "framework.rs". The file contains boilerplate code to
// create window/canvas and enter the event loop.
use winit::{self, event::WindowEvent, event_loop::ControlFlow};
use glam::UVec2;  // used for fesolution vector [u32; 2] 'res'
#[cfg(not(target_arch = "wasm32"))]
use std::future::Future;
#[cfg(not(target_arch = "wasm32"))]
use instant::{Duration, Instant};

// If rust would allow async functions in trait, the interface for using 'WgpuDriver' would be:
// pub trait WgpuLoop: 'static + Sized {
//     async fn init(window: &winit::window::Window) -> Self;
//     fn resize(&mut self, res: UVec2);
// }
// But as that is not supported, we need to import that type directly :(
use crate::WgpuDriver;

// This is implemented for user application
pub trait RenderLoop: 'static + Sized {
    fn init(driver: &WgpuDriver, screen_resolution: UVec2) -> Self;
    fn resize(&mut self, driver: &WgpuDriver, screen_resolution: UVec2);
    fn window_event(&mut self, driver: &WgpuDriver, event: WindowEvent);
    fn render(&mut self, driver: &WgpuDriver);
}

struct Window {
    window: winit::window::Window,
    event_loop: Option<winit::event_loop::EventLoop<()>>,
    size: winit::dpi::PhysicalSize<u32>,
    // The rest fields are for keeping a small pause after drawing every frame
    #[cfg(not(target_arch = "wasm32"))]
    spawner: Spawner,
    #[cfg(not(target_arch = "wasm32"))]
    skip: bool,
    #[cfg(not(target_arch = "wasm32"))]
    last_update: Instant,
    #[cfg(not(target_arch = "wasm32"))]
    idle_time: Duration,
}

impl Window {
    fn setup(title: &str) -> Window {
        #[cfg(not(target_arch = "wasm32"))]
        {
            env_logger::init();
        };

        let event_loop = winit::event_loop::EventLoop::new();
        let mut builder = winit::window::WindowBuilder::new();
        builder = builder.with_title(title);
            //.with_inner_size(winit::dpi::PhysicalSize::<u32>::from([100,5]));  // set default size
        #[cfg(windows_OFF)]
        {
            use winit::platform::windows::WindowBuilderExtWindows;
            builder = builder.with_no_redirection_bitmap(true);
        }
        let window = builder.build(&event_loop).unwrap();

        #[cfg(target_arch = "wasm32")]
        {
            use winit::platform::web::WindowExtWebSys;
            // Original file uses levels
            console_log::init().expect("could not initialize logger");
            std::panic::set_hook(Box::new(console_error_panic_hook::hook));
            // On wasm, append the canvas to the document body
            web_sys::window()
                .and_then(|win| win.document())
                .and_then(|doc| doc.body())
                .and_then(|body| {
                    body.append_child(&web_sys::Element::from(window.canvas()))
                        .ok()
                })
                .expect("couldn't append canvas to document body");
        }
        let size = window.inner_size();

        return Window {
            window,
            event_loop: Some(event_loop),
            size,
            #[cfg(not(target_arch = "wasm32"))]
            spawner: Spawner::new(),
            #[cfg(not(target_arch = "wasm32"))]
            skip: false,
            #[cfg(not(target_arch = "wasm32"))]
            last_update: Instant::now(),
            #[cfg(not(target_arch = "wasm32"))]
            idle_time: Duration::from_secs_f64(1.0 / 120.0),
        }
    }

    #[allow(unused_variables)]
    fn shedule_next_redraw(&mut self, control_flow: &mut ControlFlow) {
        #[cfg(not(target_arch = "wasm32"))]
        {
            // Idle a bit between two draw calls to free up processing power for the system.
            // Only 75% of the available processing power is used.
            if self.skip {
                self.skip = false;
                *control_flow = ControlFlow::WaitUntil(Instant::now() + self.idle_time);
            } else {
                self.skip = true;
                let timediff = self.last_update.elapsed();
                self.idle_time = timediff / 4;
                self.last_update = Instant::now();
                self.window.request_redraw();
            }
            self.spawner.run_until_stalled();
        }

        #[cfg(target_arch = "wasm32")]
        self.window.request_redraw();
    }
}

fn enter_event_loop(mut window: Window, mut driver: WgpuDriver, mut application: impl RenderLoop) {
    log::info!("Entering render loop...");
    let event_loop = window.event_loop.take().unwrap();
    event_loop.run(move |event, _, control_flow| {
        // Shorhand notations for multiple different window events
        use winit::{
            event_loop::ControlFlow::{ExitWithCode, Poll},
            event::{
                WindowEvent::{
                    Resized, ScaleFactorChanged, KeyboardInput as Key, CloseRequested as Close
                },
                Event::{WindowEvent as Win, RedrawEventsCleared, RedrawRequested},
                VirtualKeyCode::Escape as Esc,
                KeyboardInput as Inp,
            }
        };
        //let _ = (&instance, &adapter); // force ownership by the closure
        *control_flow = if cfg!(feature = "metal-auto-capture") { ExitWithCode(0) } else { Poll };  // Apple
        match event {
            RedrawEventsCleared => {
                window.shedule_next_redraw(control_flow);
            }
            Win { event: Resized(s) | ScaleFactorChanged {new_inner_size: &mut s, ..}, .. } => {
                let res = UVec2::new(s.width, s.height);  // resolution
                driver.resize(res);
                application.resize(&driver, res);
            }
            Win { event: Close | Key {input: Inp { virtual_keycode: Some(Esc), .. }, ..}, .. } => {
                 *control_flow = ExitWithCode(0);
            }
            Win { event, ..}=> {
                application.window_event(&driver, event);
            }
            RedrawRequested(_) => {
                application.render(&driver);
            }
            _ => {},
        }
    });
}

#[cfg(not(target_arch = "wasm32"))]
pub struct Spawner {
    executor: async_executor::LocalExecutor<'static>,
}

#[cfg(not(target_arch = "wasm32"))]
impl Spawner {
    fn new() -> Self {
        Self {
            executor: async_executor::LocalExecutor::new(),
        }
    }

    #[allow(dead_code)]
    pub fn spawn_local(&self, future: impl Future<Output = ()> + 'static) {
        self.executor.spawn(future).detach();
    }

    fn run_until_stalled(&self) {
        while self.executor.try_tick() {}
    }
}

#[cfg(not(target_arch = "wasm32"))]
pub fn start_event_loop<R: RenderLoop>(title: &str) {
    let window = Window::setup(title);
    // Pollster is an minimal async executor that can only evaluate a future and nothing more
    let res = UVec2::new(window.size.width, window.size.height);  // resolution
    let driver = pollster::block_on(WgpuDriver::init(&window.window, res));
    let application = R::init(&driver, res);
    enter_event_loop(window, driver, application);
}

#[cfg(target_arch = "wasm32")]
pub fn start_event_loop<R: RenderLoop>(title: &str) {
    use wasm_bindgen::{prelude::*, JsCast};

    let title = title.to_owned();
    wasm_bindgen_futures::spawn_local(async move {
        let window = Window::setup(&title);
        let res = UVec2::new(window.size.width, window.size.height);  // resolution
        let driver = WgpuDriver::init(&window.window, res).await;
        let application = R::init(&driver, res);

        let start_closure = Closure::once_into_js(
            move || enter_event_loop(window, driver, application)
        );

        // make sure to handle JS exceptions thrown inside enter_event_loop.
        // Otherwise wasm_bindgen_futures Queue would break and never handle any tasks again.
        // This is required, because winit uses JS exception for control flow to escape from `run`.
        if let Err(error) = call_catch(&start_closure) {
            let is_control_flow_exception = error.dyn_ref::<js_sys::Error>().map_or(false, |e| {
                e.message().includes("Using exceptions for control flow", 0)
            });

            if !is_control_flow_exception {
                web_sys::console::error_1(&error);
            }
        }

        #[wasm_bindgen]
        extern "C" {
            #[wasm_bindgen(catch, js_namespace = Function, js_name = "prototype.call.call")]
            fn call_catch(this: &JsValue) -> Result<(), JsValue>;
        }
    });
}
