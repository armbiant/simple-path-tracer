/// Inspired by the work of iq and fizzer:
///     https://www.shadertoy.com/view/ll3SDr
///     https://www.shadertoy.com/view/MsdGzl

type v2 = vec2<f32>;
type v3 = vec3<f32>;
type v4 = vec4<f32>;

// The amount of computation per frame is: INTERVALS * STEPS_PER_INTERVAL = 256
let STEPS_PER_INTERVAL = 32u;
let INTERVALS = 8u;  // This is upper bound for ray bounces, but the max-bounces is likely less
let PI: f32 = 3.14159265;

// Struct fields are ordered so that they do not have any padding between.
struct CameraUniform {
    rotation_matrix: mat3x3<f32>,  // Camera rotation matrix. Padding: 48
    position: v3,                  // Camera position
    time: f32,                     // Time in seconds, used for seed
    resolution: vec2<u32>,         // Resolution of image, not necessarily resolution of window!
    focus: f32,                    // Focus distance
    aperture: f32,                 // Aperture radius, depth of field, "bokeh"
    v: v4,                         // Freely tunable variable for live variable tweaking
};

struct RayInfo{  // For returning data whether ray did hit surface
    surface_data: v3,
    distance: f32,
    hit_surface: bool,
    escaped: bool,
};

struct SurfaceInfo{  // For returning data of what kind of surface ray did hit
    color: v3,
    shininess: f32,
    is_light: bool,
    is_metal: bool,
};

@group(0) @binding(0) var<uniform> cam: CameraUniform;  // Inputs from rust code each frame
@group(0) @binding(1) var inputTex: texture_2d<f32>;    // Input from last frame
@group(0) @binding(2) var outputTex: texture_storage_2d<rgba32float,write>;  // Output next frame

// Pseudorandom number generator on range [0.0, 1.0] based on fxhash
var<private> hash: u32 = 123u;  // This is seed, and it is set later with pixel coordinates
fn rnd() -> f32 {  // hash: u32
    hash = (((hash << 5u) | (hash >> 27u)) ^ 12345u) * 2654435769u;  // rotation, xor, wrapping mul.
    return f32(hash) / f32(4294967295u); // magic number is u32::MAX
}

fn square(x: f32) -> f32 { return x*x; }
fn pow5(x: f32) -> f32 { return x*square(square(x)); }  // pow(x, 5.0) is 3 times slower than this

// This gives color for ray in case it escapes the scene.
fn environment_color(dir: v3) -> v3 {
    var sky_color = v3(0.78,0.84,1.0) * (0.4 + 0.4*dir.y) * (0.4 + 0.4*dir.x); // Sky gradient
    if (dot(normalize(v3(1.0, 1.0, 0.5)), dir) > 0.99) {   // Sun
        sky_color = v3(1.0,0.93,0.88) * 50.0;  // color temperature T=5500K
    }
    return sky_color;
}

/// Fetch information from ray collision.
fn surface_info(s: v3) -> SurfaceInfo {
    let grey = v4(0.2, 0.2, 0.2, 2.0);  // Background color
    let green = v4(0.2, 0.8, 0.6, 12.0);
    let orange = v4(0.9, 0.9, 0.3, 80.0);
    var metal = false;
    var cs = grey;  // "cs" means "Color" (x,y,z) and "Shininess" (w). Negative shininess is light
    if (log2(s.y)/32.0 + s.x > 1.3) {
        cs = 13.0 * v4(1.0, 0.2, 0.3, -1.0);            // red light on nubs
    } else if (fract(cam.v.z*6.0*s.x + 0.37) > 0.9) {
        cs = v4(0.8, 0.4, 0.9, 40.0); metal = true;     // pink paint stripes
    } else if (s.y > 2.5) {                             // grey, green and orange blend
        let is_green  = clamp(sqrt(s.y)/5.0 - 0.4, 0.0, 1.0);
        let is_orange = clamp(log2(s.y) - 7.0, 0.0, 1.0);
        cs = mix(mix(grey, green, is_green), orange, is_orange);
    } else if (abs(cam.v.x*12.7*s.z - 0.1) > 0.02 && abs(cam.v.y*4.7*s.z - 0.03) < 0.02) {
        cs = 16.0 * v4(0.2, 0.5, 1.0, -1.0);            // cyan light
    }
    return SurfaceInfo(cs.xyz, cs.w, (cs.w < 0.0), metal);  // color, shininess, is_light, is_metal
}

/// Distance estimate of a 3d-fractal. This gives the geometry and the surface patterns.
/// Input: point in 3d-space. Output: distance to the object, and arbitrary iteration information
/// This is alternative version of commonly known "mandelbulb". (Btw, it's broken near the poles.)
/// http://iquilezles.org/www/articles/mandelbulb/mandelbulb.htm
fn map(p: v3) -> v4 {
    let DEG: f32 = 5.0;  // Degree of mandelbulb. If you change this, change also `r_pow_DEGminus1`
    var w: v3 = p.yzx;   // This coordinate is rotated around and around in iteration
    var r = length(w);
    var dr: f32 = 1.0;      // Derivative of the steepest gradient
    var s = v3(r, r, abs(w.x));  // For determining surface color and light. "Trapping point"
    for (var i = 0u; i < 5u; i = i + 1u) {
        let r_pow_DEGminus1 = square(square(r));  // r^(DEG-1)
        dr = DEG * r_pow_DEGminus1 * dr + 1.0;
        let a = DEG*acos(w.y / r);
        let b = DEG*atan2(w.x, w.z);
        w = p + r*r_pow_DEGminus1 * v3(sin(b)*sin(a), cos(b), sin(b)*cos(a));
        r = length(w);
        s = v3(min(s.x, r), max(s.y, r), min(s.z, abs(w.x)));
        if (r > 16.0) {break;}
    }
    return v4(0.5*log(r)*r/dr, s.x, s.y, s.z);  // The x component is distance, the rest is coloring
}

/// Calculate distance for ray to hit the object. Use raymarching: Get spehere radius that is known
/// to be empty, and move ray forward by that safe distance. Continue until sphere gets very small.
fn move_forward(ray_origin: v3, ray_dir: v3) -> RayInfo {
    var h = v4(0.0, 0.0, 0.0, 0.0);  // Nearest distance to the object (x) + iteration information (y,z,w)
    let first_dist = map(ray_origin).x;
    let min_dist = min(first_dist, 0.0008);
    var t: f32 = first_dist;    // Travelled distance on line
    // Every frame should have same initial ray, and they should save the point of impact
    for (var i = 0u; i < STEPS_PER_INTERVAL; i = i + 1u) {
        h = map(ray_origin + ray_dir*t);
        t = t + h.x;
        if (t > 5.0 || h.x < min_dist) {  // Ray escapes or hits surface
            break;
        }
    }
    return RayInfo(h.yzw, t, (h.x < min_dist), (t > 5.0));
}

/// Generate a new direction according to ideal Lambert scattering, where scattering is unaffected
/// from incoming light direction. The probability function is a cosine of angle between the
/// scattered direction and the normal. See: http://www.amietia.com/lambertnotangent.html
/// This is the same as calling `specular_scattering(-normal, normal, 1.0)`. The algorithm is:
/// 1. Generate a random point on unit sphere. 2. Place the sphere tangent to surface. 3. Normalize
fn diffuse_scattering(normal: v3) -> v3 {
    let phi = 2.0*PI*rnd();   // Using Archimedes hat-box theorem to map cylinder on a sphere
    // let u = rnd(); // OLD
    let z = 2.0*rnd() - 1.0;
    let w = sqrt(1.0 - z*z);   // Equivalent form is w = sin(acos(z)) = sin(theta)
    return normalize(normal + v3(cos(phi)*w, sin(phi)*w, z));  // the "v3" is a point on unit sphere
}

/// Generate two vectors that are orthonormal to the given one. Commonly used to rotate vectors.
/// Highly efficient algorithm invented in 2017: https://jcgt.org/published/0006/01/01/paper.pdf
fn make_orthonormal_basis(n: v3) -> array<v3,2> {  // Don't ask me how or why this works
    let s = sign(n.z);
    let a = -1.0 / (s + n.z);
    let b = n.x*n.y*a;
    return array<v3,2>(v3(1.0 + s*n.x*n.x*a, s*b, -s*n.x), v3(b, s + n.y*n.y*a, -n.y));
}

/// Generate a new direction according to Phong scattering, which applies for glossy surfaces.
/// The probability function is cos^n of the angle between scatter direction and the reflection
/// direction. See more: http://www.igorsklyar.com/system/documents/papers/4/fiscourse.comp.pdf
fn specular_scattering(dir: v3, normal: v3, n: f32) -> v3 {
    // Pick the new direction in coordinates where incoming is aligned with z'-axis (marked z2)
    let z2 = pow(rnd(), 1.0/(1.0 + n));  // n is measure of shininess, theta = acos(z)
    let p = 2.0*PI*rnd();                // phi
    let w = sqrt(1.0 - z2*z2);       // now x' = cos(p)*w and y' = sin(p)*w
    // Rotate this direction so that z-axis is around reflection direction of the normal
    let z: v3 = reflect(dir, normal);  // Reflection named as z because it is z'-axis above
    let x_y = make_orthonormal_basis(z);  // Vectors x2, y2, z2 are orthonormal
    var new_dir = x_y[0]*cos(p)*w + x_y[1]*sin(p)*w + z*z2;   // Rotate the generated direction
    if (dot(normal, new_dir) <= 0.0) {// New direction may point inwards, reflect if needed
        new_dir = reflect(new_dir, normal);
    }
    return new_dir;
}

/// Calculate normal: https://iquilezles.org/www/articles/normalsSDF/normalsSDF.htm
fn calc_normal (p: v3) -> v3 {
    let e = v2(1.0,-1.0) * 0.00001;
    return normalize(  // This clever trick requires only 4 points of tetrahedra, rather than 3*2.
        e.xyy*map(p + e.xyy).x + e.yyx*map(p + e.yyx).x +
        e.yxy*map(p + e.yxy).x + e.xxx*map(p + e.xxx).x
    );
}

/// Shoot ray for a given pixel. Includes anti aliasing, depth of field, and bloom (=lens blur).
fn shoot_ray_from_camera(pixel_pos: v2) -> array<v3,2> {
    let fov = PI / 5.0;                  // vertical field of view in radians
    let focus_distance = cam.focus;      // default 0.8
    let aperture_radius = cam.aperture;  // default 0.005

    // Pick random point on circular aperture
    let t = rnd() * 2.0 * PI;
    let lens_point: v3 = aperture_radius * sqrt(rnd()) * v3(sin(t), cos(t), 0.0);

    // Pick a point on focus plane, which is pixel grid in front of camera. Account anti aliasing
    // Using right handed coordinates: x is right, y is up, and camera is looking at -z
    let hr = 0.5 * v2(f32(cam.resolution.x), f32(cam.resolution.y));  // half resolution
    let antialiased_pos = pixel_pos - hr + v2(rnd(), rnd());
    let focus_point = v3(antialiased_pos * tan(fov / 2.0) / hr.y, -1.0) * focus_distance;

    // Shoot a ray from aperture to focus plane
    let ray_dir = normalize(focus_point - lens_point);

    // Add bloom/blur. I found that Gaussian blur can be calculated with Phong scattering cos^n(θ).
    // As n goes infinity, Phong becomes gaussian distribution N(0, σ), with n = 1/tan(σ)^2.
    let sigma = fov / (5000.0 * square(rnd())*9.0 + 10.0);  // σ is mostly negligibly narrow
    let ray_dir = specular_scattering(-ray_dir, ray_dir, 1.0/square(tan(sigma)));

    return array<v3,2>(cam.rotation_matrix * ray_dir, cam.position + lens_point);
}

/// The magic happens here: Taking x and y coordinate of an image, return the color of that pixel.
fn path_trace(pixel_pos: v2) -> v3 {
    var pixel = v3(0.0, 0.0, 0.0);      // Color of pixel as f32
    let pair = shoot_ray_from_camera(pixel_pos);  // Initial ray
    var dir = pair[0];
    var origin = pair[1];
    let dir_initial = dir;
    var origin_initial = v3(0.0, 0.0, 0.0); // Is set when ray hits surface the first time
    var mask: v3 = v3(1.0, 1.0, 1.0);   // Accumulated absorption for a ray after surface bounces
    var rays = 0;
    var first_contact = true;

    for (var i = 0u; i < INTERVALS; i = i + 1u) {
        let ray_info = move_forward(origin, dir);  // Progress ray forward, and check for collision
        origin = origin + dir * ray_info.distance;  // Update ray forward
        if (!(ray_info.escaped || ray_info.hit_surface)) {
            continue;  // Loop until something is hit, either surface or outer bounds of the scene
        }

        // Now ray has hit surface or escaped
        if (first_contact) {
            origin_initial = origin;
            first_contact = false;
        }

        // Note that if ray have escaped, value of 'surf' is garbage, and that should not be used
        let surf = surface_info(ray_info.surface_data);
        if (ray_info.escaped || surf.is_light) {
            var light_color = surf.color;  // If ray has not escaped, it hits light on the surface
            if (ray_info.escaped) {  // Ray has escaped the scene, hitting either the sun or the sky
                light_color = environment_color(dir);
            }
            pixel = pixel + mask * light_color;            // Only lights give color
            if (ray_info.escaped && rays == 0) { break; }  // Stop if initial ray points to the void
            // Start again with a new ray that is ...
            origin = origin_initial;  // ... located at a surface of first contact and
            dir = dir_initial;        // ... pointed by the pixel.
            mask = v3(1.0, 1.0, 1.0);
            rays = rays + 1;
            continue;
        }

        // Bounce the ray and update the new direction
        let normal = calc_normal(origin);
        // Fresnel coefficient with Schlick's approximation, using glass refractive index n=1.5.
        // Did you know that glass reflects as much light as any other material with same n?
        let reflected = rnd() > 0.04 + 0.96 * pow5(1.0 - max(dot(normal, -dir), 0.0));
        if (reflected) {  // The probability of reflection from n=1.5 surface
            dir = specular_scattering(dir, normal, surf.shininess);  // Specular = glossy surface
        } else {
            dir = diffuse_scattering(normal);  // Diffuse = matte surface
        }
        if (reflected || surf.is_metal) {  // Light is absorbed only if not reflected from surface
            mask = mask * surf.color;
        }
    }
    return pixel / f32(max(rays, 1));  // max avoids division 0/0
}

// Safe workgroup sizes are at least 8×16=128 and 16×16=256
@compute @workgroup_size(16, 16)
fn main(@builtin(global_invocation_id) coord: vec3<u32>) {
    if (coord.x >= cam.resolution.x || coord.x >= cam.resolution.x) {
        return;  // Leftover, as image size is less (or equal) than num-worgroups * workgroup_size.
    }
    let pixel_pos = v2(f32(coord.x) + 0.5, f32(cam.resolution.y - coord.y) - 0.5);

    // Initialize seed for random number generator
    hash = u32(pixel_pos.y) * u32(pixel_pos.x) + u32(fract(cam.time)*1000.0*pixel_pos.y);

    let pixel = path_trace(pixel_pos);
    let pixel = clamp(pixel, v3(0.0, 0.0, 0.0), v3(100.0, 100.0, 100.0));  // prevent NaNs and hardware calculation errors
    //let pixel = pow(pixel, v3(0.4545));  // WGSL assumes linear color space (so no SRGB)

    let prev_pixel = textureLoad(inputTex, vec2<i32>(coord.xy), 0);
    textureStore(outputTex, vec2<i32>(coord.xy), prev_pixel + v4(pixel, 1.0));
}


